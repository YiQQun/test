//
//  main.m
//  CoreText
//
//  Created by zhangyiqun on 16/3/9.
//  Copyright © 2016年 zhangyiqun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
